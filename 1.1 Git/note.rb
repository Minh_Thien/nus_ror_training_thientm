rebase >< merge

Các thiết lập trong file config sẽ bao gồm

    Host: tên của kết nối bạn muốn kết nối tới, tên này nên ngắn gọn dễ nhớ vì sau đó bạn sẽ gõ tên này trong lệnh ssh để kết nối thay cho các thiết lập của bạn.
    HostName : địa chỉ của server, có thể là domain hoặc địa chỉ ip
    User: login username
    IdentityFile: SSH private key của kết nối. Nếu bạn dùng ssh private key mặc định của bạn thì không cần khai báo thiết lập này.
    Port: port của kết nối, nếu server của bạn dùng port mặc định là 22 thì không cần định nghĩa thiết lập này.
    TCPKeepAlive: Có giữ kết nối ha ykhông? Nếu trong trường hợp muốn giữ kết nối thì thiết lập là yes, nếu không thì thiết lập là no
    IdentitiesOnly: Nếu cần IdentityFile thì khai báo yes, không cần thì khai báo no
    ServerAliveInterval: Nếu trong 1 khoảng thời gian mà server không gửi dữ liệu được về cho client thì báo time out theo đơn vị số giây. Ví dụ: 120 là 120s

    reset thay doi trong 1 file:  git checkout HEAD ./abc/xyz.rb

    git push origin master:login => push branch master o local len branch login tren remote.

    git push origin : login => delete branch login tren master.


    git log  => xem lai lich su commit


    git commit --amend => sua commit

    git show <...>   => Shows one or more objects (blobs, trees, tags and commits).
        git show [<options>] [<object>…​]

    git reset # git checkout => reset xoa luon tren HEAD
        --hard -> nguy hiem, co the gay mat code khi co nguoi khac push code.

    git reverse => dao nguoc lai 1 commit truoc do.

    git cherry-pick => Apply the changes introduced by some existing commits

    git push --force => push de len code ma khong can pull