http://api.jquery.com/
http://learn.jquery.com/
https://jqueryvalidation.org/

- What is it and why should we use it?
- Selectors
        All Selector (“*”)             Selects all elements.
        Class Selector (“.class”)      Selects all elements with the given class.
        Element Selector (“element”)   Selects all elements with the given tag name.
        ID Selector ("#id")            Selects a single element with the given id attribute.
        Multiple Selector (“selector1, selector2, selectorN”)                    Selects the combined results of all the specified selectors.

- Filters (Selectors → Filters, Traversing → Filtering)
- DOM Manipulation Methods
    append()            Inserts content to the end of element(s) which is specified by a selector.
    before()            Inserts content (new or existing DOM elements) before an element(s) which is specified by a selector.
    after()             Inserts content (new or existing DOM elements) after an element(s) which is specified by a selector.
    prepend()           Insert content at the beginning of an element(s) specified by a selector.
    remove()            Removes element(s) from DOM which is specified by selector.
    replaceAll()        Replace target element(s) with specified element.
    wrap()              Wrap an HTML structure around each element which is specified by selector.

- Methods:
  + each:   Iterate over a jQuery object, executing a function for each matched element.
            $( "li" ).each(function( index ) {
              console.log( index + ": " + $( this ).text() );
            });
  + filter: Reduce the set of matched elements to those that match the selector or pass the function*s test.
            $( "li" ).filter( ":even" ).css( "background-color", "red" );


  + map: .map(callback) Pass each element in the current matched set through a function, producing a new jQuery object containing the return values.
  + size: size() Return the number of elements in the jQuery object.
  + data: Store arbitrary data associated with the specified element. Returns the value that was set.
          jQuery.data( element, key, value )
  + attr:
        .attr( attributeName )
        Get the value of an attribute for the first element in the set of matched elements.

        .attr( attributeName,  value )
        Set one or more attributes for the set of matched elements.

- Effects
    Basic: Hide/show
    Siding: slideUp()/slideDown()/slideToggle
    Fading: fadeIn()/ fadeOut()/ fadeTo()
    animate
    stop
    Callbacks
    chaining

- Events
  + jQuery Event Basics ( on, off, data, bind, change, click, delegate, event, hover, focus, keydown, keypress, keyup, load, mousedown, mouseenter, mouseleave, mouseout, ready, scroll, select, submit, toggle, hide, show, trigger, bind, unbind, unload)

        on -> Attach multiple event handlers
        off() -> Remove an event handler
        bind -> Attach a handler to an event for the elements.
            $( "#foo" ).bind( "click", function() {
               alert( "User clicked on 'foo.'" );
            });
        change -> Bind an event handler to the "change" JavaScript event, or trigger that event on an element. This event is limited to <input> elements, <textarea> boxes and <select> elements.
        click ->  Bind an event handler to the "click" JavaScript event, or trigger that event on an element.
        delegate -> on()

        keyup (sau khi an phim) >< keypress (an vao phim)

        hover

        mouseenter - mouseleave

        mouseup - mousedown

        mouseout


- Ajax
  + What is it used for?
  + Ajax setup (jQuery’s Ajax-Related Methods)
  + Options (jQuery’s Ajax-Related Methods)
  + Callbacks (success – done, fail - error, complete - always)
- Jquery validate plugin
  + What is it used for?
  + Plugin methods (validate, rules, valid, messages)






  Note sau review:
  .find()
  .closest()
  .remove() -> xoa element khoi DOM (ca element child node)
  .empty() -> xoa child node
  .html()
  -data handler






