- Configure to send emails in Rails
  + Introduction
  + Action Mailer Configuration
  + Example Action Mailer Configuration
  + Action Mailer Configuration for Gmail
- Send email
  + Sending Emails
  + Walkthrough to Generating a Mailer
  + Sending Email To Multiple Recipients
  + Sending Email With Name
  + Mailer Views
  + Action Mailer Layouts
  + Generating URLs in Action Mailer Views
  + Adding images in Action Mailer Views
  + Sending Emails without Template Rendering