- What is it and why should we use it?
      Bootstrap makes front-end web development faster and easier. It is made for folks of all skill levels, devices of all shapes, and projects of all sizes
      Bootstrap is the most popular HTML, CSS, and JavaScript framework for developing responsive, mobile-first websites.

- Grid system
 + Introduction
      Use our powerful mobile-first flexbox grid to build layouts of all shapes and sizes thanks to a twelve column system, five default responsive tiers, Sass variables and mixins, and dozens of predefined classes.
 + Media queries
      @media (max-width: @screen-xs-max) { ... }
      @media (min-width: @screen-sm-min) and (max-width: @screen-sm-max) { ... }
      @media (min-width: @screen-md-min) and (max-width: @screen-md-max) { ... }
      @media (min-width: @screen-lg-min) { ... }

 + Grid options
      .col-     .col-sm-     .col-md-     .col-lg-           col-xl
      <576px    >=576px       >=768px      >=992px           >=1200px

 + Alignment classes

- Predefined classes in Typography
  + Typography

   Documentation and examples for Bootstrap typography, including global settings, headings, body text, lists, and more.
        Abbreviations (viet tat)

- Tables
 + Basic
 + Responsive tables
- Forms
 + Basic
 + Inline form
 + Horizontal form
 + Supported controls
- Buttons
 + Button tags
 + Options
 + Size
- Image
- Bootstrap Components:
 + Glyphicons (How to use-example)
 + Dropdowns (Example (understand)
 + Button dropdowns (Single button dropdowns)
 + Navbar (Default, Brand, Form, Fixed to top)
 + Pagination (Default pagination)
 + Alerts (Examples, Dismissible alerts)
- Javascript components
  + modal
  + dropdown
  + alert
  + collapse
- Bootstrap datepicker