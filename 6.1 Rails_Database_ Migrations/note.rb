

- What is it, what is it for
  + Migration Overview
- Create migration (change, up, down...)
  + Creating a Standalone Migration
  + Model Generators
  + Creating a Table
  + Creating a Join Table
  + Changing Columns
  + Column Modifiers
  + Foreign Keys
  + When Helpers are not Enough
  + Using the change Method
  + Using reversible
  + Using the up/down Methods
- Run migration + Rollback migration
  + Running Migrations
  + Rolling Back
  + Setup the Database
  + Resetting the Database
  + Running Specific Migrations
  + Running Migrations in Different Environments
- Rule to update/delete migration
  + Changing Existing Migrations



  reverse the migration automatically :

    add_column
    add_foreign_key
    add_index
    add_reference
    add_timestamps
    change_column_default (must supply a :from and :to option)
    change_column_null
    create_join_table
    create_table
    disable_extension
    drop_join_table
    drop_table (must supply a block)
    enable_extension
    remove_column (must supply a type)
    remove_foreign_key (must supply a second table)
    remove_index
    remove_reference
    remove_timestamps
    rename_column
    rename_index
    rename_table

    db:migrate:up VERSION=     -> chi chay dung version duoc goi.