--> Action controller

- Define controller with naming convention
  + What Does a Controller Do?
  + Controller Naming Convention
- Difference between Action and Method
  + Methods and Actions
- Follow RESTful to build controller actions
  +  http://guides.rubyonrails.org/routing.html#crud-verbs-and-actions
- HTTP Verbs
  + https://en.wikipedia.org/wiki/Representational_state_transfer
  + http://guides.rubyonrails.org/routing.html#crud-verbs-and-actions
- Parameters
- Sessions, flash, cookies
  + Session
  + Cookies
- Filter
  + Filters
- Layout and rendering
  + Overview: How the Pieces Fit Together
  + Creating Responses
  + Rendering by Default: Convention Over Configuration in Action
  + Using render
  + Rendering an Action's View
  + Rendering an Action's Template from Another Controller
  + Rendering an Arbitrary File
  + Wrapping it up
  + Using redirect_to
  + Getting a Different Redirect Status Code
  + The Difference Between render and redirect_to
- Specify Layout
  + Options for render (The :layout Option)
  + Finding Layouts
  + Specifying Layouts for Controllers
  + Choosing Layouts at Runtime
  + Conditional Layouts
  + Layout Inheritance


  ====================================

  - Create views for actions
  + What is Action View?
  + Using Action View with Rails
  + Templates, Partials and Layouts
  + Templates
  + ERB
- Partial
  + Partials
  + Naming Partials
  + Using Partials to simplify Views
  + Render without partial and locals options
  + Rendering Collections
  + Partial Layouts
- Form helpers
  + FormHelper
  + FormOptionsHelper
  + FormTagHelper
- Action view helpers: number, currency, text...
  + NumberHelper
- Other formats besides HTML
  + https://apidock.com/rails/ActionController/MimeResponds/InstanceMethods/respond_to
- SLIM
  + What is Slim?
  + Why use Slim?
  + How to start?
  + Syntax example
  + Verbatim text |
  + Verbatim text with trailing white space ''
  + Control code -
  + Output =
  + Code comment /
  + Trailing and leading whitespace
  + Text content
  + Dynamic content (= and ==)
  + Attributes
  + Attributes wrapper
  + Quoted attributes
  + Ruby attributes
  + Boolean attributes
  + ID shortcut # and class shortcut .
  + Text interpolation
  + Embedded engines (Markdown, …)