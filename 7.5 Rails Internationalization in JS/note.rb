- Why should we use it?
- How to use: normal translation, translation with parameters
  + Installation
  + Rails app
  + Rails app with Asset Pipeline
  + Export Configuration (For translations)
  + Fallbacks
  + Setting up
  + Number formatting
  + Date formatting
- Set default language, change language
  + Setting up