--> Rails Routing from the Outside In

- What is it, what is it for
  + The Purpose of the Rails Router
- Define resource with resourceful + RESTful
  + Resource Routing: the Rails Default
  + Resources on the Web
  + CRUD, Verbs, and Actions
  + Path and URL Helpers
  + Defining Multiple Resources at the Same Time
  + Singular Resources
  + Creating Paths and URLs From Objects
  + Adding More RESTful Actions
  + Adding Member Routes
  + Adding Collection Routes
  + Restricting the Routes Created
  + Listing Existing Routes
  + Creating Paths and URLs From Objects
- Non-resourceful actions
  + Non-Resourceful Routes
  + Bound Parameters
  + Dynamic Segments
  + Static Segments
  + The Query String
  + Naming Routes
  + HTTP Verb Constraints
- Nested resources
  + Nested Resources
- Namespace
  + Controller Namespaces and Routing
- Root, root of namespace
  + Using root
