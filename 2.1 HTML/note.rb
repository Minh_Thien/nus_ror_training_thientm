- Basic

  + HTML Home
      With HTML you can create your own Website.

  + HTML Introdution
      HTML is the standard markup language for creating Web pages.
      HTML stands for Hyper Text Markup Language
      HTML describes the structure of Web pages using markup
      HTML elements are the building blocks of HTML pages
      HTML elements are represented by tags
      HTML tags label pieces of content such as "heading", "paragraph", "table", and so on
      Browsers do not display the HTML tags, but use them to render the content of the page


  + HTML Basic
      All HTML documents must start with a document type declaration: <!DOCTYPE html>.
      The HTML document itself begins with <html> and ends with </html>.
      The visible part of the HTML document is between <body> and </body>.


- HTML Elements (tags)
  + html, body, head
  + header, footer

  + a : link,
      href: URL
      target: _blank
              _parent
              _self
              _top

  + area : defines an area inside an image-map (an image-map is an image with clickable areas).

  + article : An article should make sense on its own and it should be possible to distribute it independently from the rest of the site.

  + audio : <audio controls>
              <source src="horse.ogg" type="audio/ogg">
              <source src="horse.mp3" type="audio/mpeg">
              Your browser does not support the audio tag.
            </audio>

        MP3 audio    /mpeg
        OGG audio    /ogg
        WAV audio    /wav

  + code :
        <em>  Renders as emphasized text
        <strong>  Defines important text
        <code>  Defines a piece of computer code
        <samp>  Defines sample output from a computer program
        <kbd> Defines keyboard input
        <var> Defines a variable

  + div : defines a division or a section in an HTML document.
          The <div> element is often used as a container for other HTML elements to style them with CSS or to perform certain tasks with JavaScript.

  + embed : defines a container for an external application or interactive content (a plug-in).

  + form : is used to create an HTML form for user input.
        The <form> element can contain one or more of the following form elements:

            <input>
            <textarea>
            <button>
            <select>
            <option>
            <optgroup>
            <fieldset>
            <label>

  + iframe :

  + h1-> h6, b, br,

  + button
      type  button
            reset
            submit

  + canvas : tag is used to draw graphics, on the fly, via scripting (usually JavaScript).

  + img,
  + input (type ="checkbox, radio")
  + label, li, link, menu, ol, select, option
  + strong, table, th, td, tr, u, ul, video
  + style, script


- HTML Attributes
  + id, class and common attributes of above tags

  =>Thuoc tinh name="abc" la ten parameter truyen len server khi goi request.

