http://www.ruby-lang.org/en/libraries/


- What is rubygem
    Như hầu hết các ngôn ngữ lập trình, Ruby sử dụng một tập các thư viện của bên thứ ba.
    Gần như tất cả các thư viện này được phát hành dưới dạng một gem, một goi thư viện hoặc ứng dụng có thể được cài đặt với một công cụ gọi là RubyGems.
    RubyGems là một hệ thống đóng gói Ruby được thiết kế để tạo thuận lợi cho việc tạo, chia sẻ và cài đặt các thư viện

- How to manage gems and use a gem
  + Install/Remove gems

    Nơi chính nơi các thư viện được lưu trữ là RubyGems.org, một kho lưu trữ gem công cộng có thể được tìm kiếm và cài đặt trên máy của bạn. Bạn có thể duyệt và tìm kiếm các đá quý bằng cách sử dụng trang web RubyGems hoặc sử dụng lệnh gem.

  + Bundler

    Bundler provides a consistent environment for Ruby projects by tracking and installing the exact gems and versions that are needed.