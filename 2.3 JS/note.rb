- Basic
  + JS Home

  + JS Introduction

- Methods of adding JS to HTML
  + JS Where to
        The <script> Tag
        JavaScript in <head> or <body>
        External JavaScript
        External References

- JavaScript can "display" data in different ways:

    Writing into an HTML element, using innerHTML.
    Writing into the HTML output using document.write().
    Writing into an alert box, using window.alert().
    Writing into the browser console, using console.log().

- Syntax

    *The variables lastName and lastname, are two different variables.

    x += y       =>      x = x + y

    &&      logical and
    ||      logical or
    !       logical not

    &       AND          Sets each bit to 1 if both bits are 1
    |       OR           Sets each bit to 1 if one of two bits is 1
    ^       XOR          Sets each bit to 1 if only one of two bits is 1
    ~       NOT



- Structure control
 + JS Conditions
      if (condition) {
          block of code to be executed if the condition is true
      } else {
          block of code to be executed if the condition is false
      }

 + JS Switch
      switch(expression) {
          case x:
              code block
              break;
          case y:
              code block
              break;
          default:
              code block
      }

- Loop
  + JS Loop For
  + JS Loop While
  + JS Break
- Data types
  + JS Data types ( Numbers, Object, String, Array, Date)

      * JavaScript Types are Dynamic
      * JavaScript Strings             "Thien"
      * JavaScript Numbers             11   11.23    12e5
      * JavaScript Booleans            true false
      * JavaScript Arrays              var cars = ["Saab", "Volvo", "BMW"];
      * JavaScript Objects

- Operators
  + JS Operators
- Object scope (this)
  + JS Functions
  + JS Opjects
  + JS Events



  var abc = [1, 2, 3];

  var person = {name: "thien", age:"12"};

  person[0];


************ phan biet toan tu tro toi vung nho va tao vung nho moi **************





