- Why should we use it?
- How to use: normal translation, translation with parameters, lazy lookup
  + The Public I18n API
  + Internationalization and Localization
  + Abstracting Localized Code
  + Providing Translations for Internationalized Strings
  + Passing Variables to Translations
  + Adding Date/Time Formats
  + Organization of Locale Files
  + Overview of the I18n API Features
  + Basic Lookup, Scopes and Nested Keys
  + "Lazy" Lookup
  + Pluralization
  + Setting and Passing a Locale
- Set default language, change language
  + Setup the Rails Application for Internationalization
  + Configure the I18n Module
  + Managing the Locale across Requests
  + Setting the Locale from the Domain Name
  + Setting the Locale from URL Params
  + Setting the Locale from User Preferences
  + Storing the Locale from the Session or Cookies

  http://guides.rubyonrails.org/i18n.html