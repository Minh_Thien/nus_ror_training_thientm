$ ->
  $(".edit_article").validate {
    rules:
      'article[title]': {
        required: true,
        minlength: 5
      },
      'article[text]': {
        required: true,
        minlength: 10
      }
  }

