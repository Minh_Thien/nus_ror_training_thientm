class Article < ApplicationRecord
  has_and_belongs_to_many :categories
  belongs_to :author

  # validates :title, presence: true,
  #                   length: { minimum: 5 }
  # validates :text, presence: true,
  #                   length: { minimum: 10 }

end
