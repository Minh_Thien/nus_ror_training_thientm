class Admin < ApplicationRecord
  validate :can_access_18

  def can_access_18
    unless is_admin && age <18
      self.can_access = false
    end
    self.can_access = true
  end
end