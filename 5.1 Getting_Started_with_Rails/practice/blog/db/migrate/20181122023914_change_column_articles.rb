class ChangeColumnArticles < ActiveRecord::Migration[5.2]
  def change
    remove_column :articles, :active, :boolean
    add_column :articles, :gender, :string
  end
end
