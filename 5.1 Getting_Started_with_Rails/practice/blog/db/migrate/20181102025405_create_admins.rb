class CreateAdmins < ActiveRecord::Migration[5.2]
  def change
    create_table :admins do |t|
      t.boolean :is_admin
      t.string :adress
      t.integer :age
      t.boolean :can_access
    end
  end
end
