class ChangeYearFromArticles < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        change_column :articles, :year, :string
      end
      dir.down do
        change_column :articles, :year, :integer
      end
    end
  end
end
