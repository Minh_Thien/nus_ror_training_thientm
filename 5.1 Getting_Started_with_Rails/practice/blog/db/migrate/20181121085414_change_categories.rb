class ChangeCategories < ActiveRecord::Migration[5.2]
  def change
    remove_column :categories, :music, :boolean
    remove_column :categories, :post, :boolean
    remove_column :categories, :story, :boolean
    add_column :categories, :type, :string

  end
end
