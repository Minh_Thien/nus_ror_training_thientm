http://guides.rubyonrails.org/getting_started.html
http://guides.rubyonrails.org/configuring.html


Give an overview about Rails
- Create new RAILS application
  + What is Rails?

  + Creating a New Rails Project

      rails new <blog>


- How to start it
  + Hello, Rails!
- MVC in Rails
  + Getting Up and Running

- Directory structure
  +  Creating the Blog Application

- Basic configuration: database, environment
  + Locations for Initialization Code
  + Running Code Before Rails
  + Configuring Rails Components: 3.1, 3.2, 3.5, 3.15, 3.16
