{
  "color_scheme": "Packages/Color Scheme - Default/Mariana.tmTheme",
  "detect_indentation": false,
  "file_exclude_patterns":
  [
    "bundle.js",
    "public/javascripts/translations.js"
  ],
  "folder_exclude_patterns":
  [
    ".svn",
    ".git",
    ".hg",
    "CVS",
    "i18n",
    "log",
    "tmp/cache",
    "tmp/data",
    "tmp/rubycritic",
    "tmp/miniprofiler",
    "coverage",
    "public/system/images",
    "public/packs",
    "node_modules",
    ".happypack"
  ],
  "font_size": 9,
  "highlight_line": true,
  "ignored_packages":
  [
    "Vintage"
  ],
  "scroll_past_end": false,
  "tab_size": 2,
  "translate_tabs_to_spaces": true,
  "trim_trailing_white_space_on_save": true,
  "word_wrap": true
}
