http://www.tutorialspoint.com/ruby/index.htm


- Overview
 + Ruby Overview

- Syntax
 + Ruby synstax


- Class
 + Ruby - Classes and Objects
- Method

 + Ruby - Methods
- Data types:
  + Ruby - Strings (http://ruby-doc.org/core-2.5.0/String.html)
  + Ruby - Arrays (http://ruby-doc.org/core-2.5.0/Array.html)
  + Ruby - Hashs (http://ruby-doc.org/core-2.5.0/Hash.html)
  + Ruby - Date & Time (https://ruby-doc.org/stdlib-2.3.1/libdoc/date/rdoc/DateTime.html)
- Variables
 + Ruby variables



- Operators
 + Ruby - Operators
- Structure control
 + Ruby if-else
1/
      if conditional [then]
         code...
      [elsif conditional [then]
         code...]...
      [else
         code...]
      end

2/
      code if condition

3/
      unless conditional [then]
         code
      [else
         code ]
      end

4/
      code unless conditional

5/
    case expr0
      when expr1, expr2
         stmt1
      when expr3, expr4
         stmt2
      else
         stmt3
      end

- Loop
 + Ruby loop

1/
      while conditional [do]
         code
      end

2/

      code while condition

      OR

      begin
        code
      end while conditional

3/
      until conditional [do]
         code
      end

4/

      code until conditional

      OR

      begin
         code
      end until conditional

5/

    for variable [, variable ...] in expression [do]
       code
    end

6/

(expression).each do |variable[, variable...]| code end

- Module
 + Ruby modules
- Exception management
 + Ruby - Exceptions
- Object oriented
 + Ruby - Object Oriented

==> note test
- 4 ham: shift, unshift, pop, push
- bien instance # class, global # local
- .zero?
- each # map
- cach dat ten function, class, variable
- duyet qua phan tu mang dung .each
- .john # split
- .flatten
- class function, instance function
- class function chi co the thay doi class variable,...
