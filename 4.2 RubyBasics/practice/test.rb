class Driver
  public
  def public_method
    puts "public_method called"

    private_method()
    protected_method()

    # self.private_method()
    self.protected_method()

    d = Driver.new
    # d.private_method()
    d.protected_method()
  end

  private
  def private_method
    puts "private_method called"
  end

  protected
  def protected_method
    puts "protected_method called"
  end
end

d = Driver.new
d.private_method()
d.protected_method()
d.public_method()




# class Driver
#   def func1
#     puts "func1"
#   end

# end

# class Driver
#   def func2
#     puts "func2"
#   end
# end

# d = Driver.new
# d.func1

# def promptAndGet(prompt)
#    print prompt
#    res = readline.chomp
#    throw :quitRequested if res == "!"
#    return res
# end

# catch :quitRequested do
#    name = promptAndGet("Name: ")
#    age = promptAndGet("Age: ")
#    sex = promptAndGet("Sex: ")
#    # ..
#    # process information
# end
