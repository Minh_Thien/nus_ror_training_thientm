

- Use predefined validations
  + Why Use Validations?
  + When Does Validation Happen?
  + Skipping Validations
  + valid? and invalid?
  + errors[]
  + errors.details
  + Validation Helpers
  + Common Validation Options
- Validate with “if” or “unless”
  + Conditional Validation
- Custom validation
  + Performing Custom Validations
- Lits of callbacks, when are they called
  + The Object Life Cycle
  + Callbacks Overview
  + Available Callbacks
  + Running Callbacks
  + Skipping Callbacks
  + Conditional Callbacks
  + Transaction Callbacks
- Order of callbacks
  + Available Callbacks
- When callbacks cause Transaction Rollback or cancelled?
  + Halting Execution

--->mot column trong table ung voi mot attribute trong class, nguoc lai chua chac
--->

- Relation types
  + The Types of Associations
- Create relations by conventions
  + The Types of Associations
- Create relations when table name, field name don't follow conventions
  + belongs_to Association Reference
  + has_one Association Reference
  + has_many Association Reference
  + has_and_belongs_to_many Association Reference
- Create relations with condition (where, order, etc...)
  + Scopes for [association's name]
- Association benefit (IMPORTANT)
  + Why Associations?