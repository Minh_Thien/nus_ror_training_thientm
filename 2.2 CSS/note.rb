
- Syntax
  + CSS Syntax : A CSS rule-set consists of a selector and a declaration block

- ID & class
  + CSS Selectors (.class, #id)

- Methods of adding CSS to HTML
  + CSS How To
    External style sheet : <link rel="stylesheet" type="text/css" href="mystyle.css">
    Internal style sheet : <style>
                                p {

                                }
                           </style>
    Inline style : <h1 style="color:blue;margin-left:30px;">This is a heading</h1>

- Style priority
    Inline style > ID selectors – (#big) > Pseudo-classes > Attributes selectors – [href], [target] > Class selectors – (.class-selector) > Type selectors – (em, h1, p, ul, li) > Universal selectors – (*)


- CSS Selectors
  + .class, #id,

  + element, element,element, element element, element>element,

        .class              .intro         Selects all elements with class="intro"
        #id                 #firstname     Selects the element with id="firstname"
        *                   *              Selects all elements
        element             p              Selects all <p> elements
        element,element     div, p         Selects all <div> elements and all <p> elements
        element element     div p          Selects all <p> elements inside <div> elements
        element>element     div > p        Selects all <p> elements where the parent is a <div> element
        element+element     div + p        Selects all <p> elements that are placed immediately after <div> elements
        element1~element2   p ~ ul         Selects every <ul> element that are preceded by a <p> element

  + [attribute], [attribute=value], [attribute~=value], [attribute^=value], [attribute*=value]

        [attribute]                [target]                Selects all elements with a target attribute
        [attribute=value]          [target=_blank]         Selects all elements with target="_blank"
        [attribute~=value]         [title~=flower]         Selects all elements with a title attribute containing the word "flower"
        [attribute|=value]         [lang|=en]              Selects all elements with a lang attribute value starting with "en"
        [attribute^=value]         a[href^="https"]        Selects every <a> element whose href attribute value begins with "https"
        [attribute$=value]         a[href$=".pdf"]         Selects every <a> element whose href attribute value ends with ".pdf"
        [attribute*=value]         a[href*="w3schools"]    Selects every <a> element whose href attribute value contains the substring "w3schools"


- CSS Properties
  + animation

        animation-name                   Specifies the name of the keyframe you want to bind to the selector
        animation-duration               Specifies how many seconds or milliseconds an animation takes to complete
        animation-timing-function        Specifies the speed curve of the animation
        animation-delay                  Specifies a delay before the animation will start
        animation-iteration-count        Specifies how many times an animation should be played
        animation-direction              Specifies whether or not the animation should play in reverse on alternate cycles
        animation-fill-mode              Specifies what values are applied by the animation outside the time it is executing
        animation-play-state             Specifies whether the animation is running or paused
        initial                          Sets this property to its default value. Read about initial
        inherit                          Inherits this property from its parent element. Read about inherit

  + background (-image, -color, -size..), border (-bottom, -top, -color...all)
  + color, cursor (hinh thuc tro chuot), display, flex
  + @font-face, font(-size, -family, -weight, -style...)
  + with, height, left, right, float, top, bottom,
  + line-height, list-style
  + margin, opacity (lam mo anh), overflow (overflow: visible|hidden|scroll|auto|initial|inherit;) , padding, position


- CSS pseudo element/class
  + :link, :visited, :hover, :active
  + :active, ::after, ::before, :checked, :disabled, :empty
  + :first-child, ::first-letter, :first-of-type, :last-child, :last-of-type
  + :focus, :hover, :link, :not(selector),
  + :nth-child(n), :nth-last-child(n), :nth-last-of-type(n), :nth-of-type(n),"







